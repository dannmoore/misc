#include <iostream>
#include <array>
#include <queue>
#include <vector>

#define NUM_ROOMS 4



// PrintPath() - Print the path from startRoom to endRoom. Not part of the coding challenge but
//  useful for debugging.
template<std::size_t SIZE>
void PrintPath(std::array<int, SIZE> path, int startRoom, int endRoom) {
	std::vector<int> final_path;

	int currentRoom = endRoom; // Start at the ending room
	final_path.push_back(endRoom);
	while (currentRoom != startRoom) {
		currentRoom = path[currentRoom]; // Follow the path back by updating currentRoom to the index pointed at in path
		final_path.push_back(currentRoom);
	}

	std::cout << "Path Taken: ";
	// Now print in reverse order
	for (int i = final_path.size() - 1; i >= 0; i--) {
		std::cout << final_path[i] << " ";
	}
	std::cout << "\n";
}

// CanBeReached() - Determine if the endRoom can be reached from the startRoom given a matrix
//  that holds information about which rooms are connected to each other
template<std::size_t SIZE>
static bool CanBeReached(int startRoom, int endRoom, std::array<std::array<bool, SIZE>, SIZE> rooms) {
	// The room array is a graph that can be traversed by a simple breadth-first search

	std::queue<int> q; // Create a queue of integers of rooms to check
	std::array<int, SIZE> path; // Create an array of indicies for each room to hold where it was visited from
	path.fill(-1); // Fill the array such that no room has yet been visisted (-1 = not visited yet)

	// Sanity checks for out of scope indicies
	if (startRoom < 0 || endRoom < 0) return(false);
	if (startRoom > SIZE - 1 || endRoom > SIZE - 1) return(false);

	// Push our starting room onto the queue
	q.push(startRoom);

	path[startRoom] = startRoom; // The starting room always points to itself 

	int currentRoom = -1;
	// Continue looping until the queue is empty
	while (q.size() > 0) {
		currentRoom = q.front(); // Get the room from the front of the queue
		q.pop(); // Pop the first room off of the queue

		if (currentRoom == endRoom) {
			// We have found the end room
			PrintPath(path, startRoom, endRoom); // DEBUG: Print path
			return(true);
		}

		// Check other rooms to see if we can travel from currentRoom to there
		for (int i = 0; i < NUM_ROOMS; i++) {
			if (rooms[currentRoom][i]) { // Can travel?
				// Can only travel if the room has not already been visited
				if (path[i] == -1) {
					// Add this room to the queue
					q.push(i);

					// Update the path for this room to point as coming from the current room
					path[i] = currentRoom;
				}
			}
		}
	}

	return(false);

	// Note: 
	//  You could also use A* pathfinding, especially if cost to move between rooms varies

}




int main() {
	std::array<std::array<bool, NUM_ROOMS>, NUM_ROOMS> rooms = { { {false, true, false, false},
																   {false, false, true, false},
																   {true, false, false, true},
																   {false, false, false, false} } };



	if (CanBeReached(0, 3, rooms)) {
		std::cout << "True\n";
	}
	else {
		std::cout << "False\n";
	}


	std::cout << "\n";
	system("pause");
}