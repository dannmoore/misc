# Rooms BFS

### Coding Challenge:

You are working on a new game with procedurally generated rooms. It is important that players can get from the starting point to the finish point on each map.
You have been given an array of boolean values which represents all of the rooms in your map and whether or not they connect to each other room (rooms never connect to themselves).

Example:
The following array should be read as follows: rooms[x][y] = true if there exists a path from room x to room y, false otherwise

```
#define NUM_ROOMS 4
std::array<std::array<bool, NUM_ROOMS>, NUM_ROOMS> rooms = { { {false, true, false, false},
{false, false, true, false},
{true, false, false, true},
{false, false, false, false} } };
```

For the value of 'rooms' above:
- The player can start in room 0 and reach room 3 via the following path: 0 -> 1 -> 2 -> 3
- The player can not go from room 3 to room 1.

Given this information, write a function that determines if players can get from a given starting room to a given end room.  This function will constantly be invoked during the level with each level creating a new instance of this class. Optimize for speed accordingly.


OUTPUT:
True if a path exists from start to end, otherwise false.


### Legal:
Copyright (C) 2022 Dann Moore.  All rights reserved.