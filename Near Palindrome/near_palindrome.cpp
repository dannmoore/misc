#include <iostream>
#include <string>
#include <fstream>

// IsPalindrome() - Check whether the given string is a palindrome
bool IsPalindrome(std::string s) {
	// Sanity check, an empty string is not a palindrome
	if (s.length() == 0) {
		return(false);
	}

	// Create a loop of two "pointers", one at the start and one at the end of the string
	// Each loop we increment the head and decrement the tail
	// When the two pointers "pass" each other we exit the loop
	for(int i = 0, j = s.length() - 1; i <= j; i++, j--) {
		// Check if the character at our head and tail pointer is not identical
		if (s[i] != s[j]) { // If a single difference is found it is not a palindrome
			return(false);
		}
	}

	// No differences were found, so this string is a palindrome
	return(true);
}


// IsPalindromeWithChange() - Check whether the given string is a palindrome with a single letter change
bool IsPalindromeWithChange(std::string s) {
	// Sanity check, an empty string cannot be a palindrome
	if (s.length() == 0) {
		return(false);
	}

	int count = 0; // Create a counter for the number of differences found in the string

	// Create a loop of two "pointers", one at the start and one at the end of the string
	// Each loop we increment the head and decrement the tail
	// When the two pointers "pass" each other we exit the loop
	for (int i = 0, j = s.length() - 1; i <= j; i++, j--) {
		// Check if the character at our head and tail pointer is not identical
		if (s[i] != s[j]) { // If a letter difference is found increment the counter
			count++;
			// If more than one difference is found then this is NOT a near palindrome with a changed letter
			if (count > 1) {
				return(false);
			}
		}
	}

	// No more than one difference was found, so this string is a near palindrome
	return(true);
}


// IsPalindromeWithDelete() - Check whether the given string is a palindrome with a single letter deleted
bool IsPalindromeWithDelete(std::string s) {
	// Sanity check, an empty string cannot be a palindrome
	if (s.length() == 0) {
		return(false);
	}

	// Test each letter in the string to see if a palindrome will exist when the letter is removed
	// To do this we will select an index to be deleted and skip over it during our loop
	for (unsigned int deleteindex = 0; deleteindex < s.length(); deleteindex++) {
		// Create a loop of two "pointers", one at the start and one at the end of the string
		// Each loop we increment the head and decrement the tail
		// When the two pointers "pass" each other we exit the loop
		int i = 0;
		int j = s.length() - 1;
		bool diff = false;
		while (i <= j && !diff) {
			// If the deleteindex matches one of our pointers, skip over that letter
			if (deleteindex == i) i++;
			if (deleteindex == j) j--;

			// Sanity check, break out of loop if pointers pass each other 
			if (j < i) {
				break;
			}

			// Check if the character at our head and tail pointer is not identical
			if (s[i] != s[j]) { // If a single difference is found it can't be a near palindrome
				diff = true;
			}

			i++;
			j--;
		}

		if (!diff) {
			// By deleting the current letter at deleteindex we have found a palindrome
			return(true);
		}
	}

	// After testing each letter being deleted, we couldn't find a palindrome
	return(false);

	// Notes:
	//  We could have called our helper function IsPalindrome() but that would be slightly less efficient as
	//  new strings would need to be created with each missing letter to then be checked
}




// CheckNearPalindrome() - Checks the given string to see if it is a "near" palindrome
bool CheckNearPalindrome(std::string s) {
	// There are three basic checks we need to do to determine if the given string is a
	//  "near" palindrome.
	// 1. Is the string already a palindrome
	// 2. Is the string a palindrome if a single letter can be REPLACED (changed)
	// 3. Is the string a palindrome if one letter is REMOVED (deleted)
	// 3a. Is the string a palindrome if one letter is ADDED (inserted).  By definition this is the
	//     same as deleting a single letter -- If you can add a letter to create a palindrome then you
	//     could just as easily delete the cooresponding letter on the other side of the string as well
	//

	// First let's check if the string is already a palindrome
	if (IsPalindrome(s)) {
		return(true);
	}

	// Next, let's check if the string could be a palindrome if a single letter could be changed
	if (IsPalindromeWithChange(s)) {
		return(true);
	}

	// Next, let's check if the string could be a palindrome if a single letter is removed
	if (IsPalindromeWithDelete(s)) {
		return(true);
	}

	return(false); // Failed all tests

	// Notes: 
	//  This example will fail if the characters are not all upper or lowercase... 
	//  Additional checks would need to be made to determine if characters with
	//  different cases are the same.
}


int main() {
	std::ifstream inputfile(".\\data\\Test_1_input.txt");
	int n;
	inputfile >> n; inputfile.ignore();
	for (int i = 0; i < n; i++) {
		std::string word;
		std::getline(inputfile, word);

		if (CheckNearPalindrome(word)) {
			std::cout << "1";
		}
		else {
			std::cout << "0";
		}
	}

	std::cout << "\n";
	system("pause");
}


