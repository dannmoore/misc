# Near Palindrome

### Coding Challenge:

A palindrome is a word which reads the same backward as forward, such as "madam" or "racecar".

A near-palindrome is a word one letter away from being a palindrome, be it by replacement, addition or removal.

Write a program that computes whether words are near-palindrome or not.

INPUT:
Line 1: The number of words N
Next N lines: a word

OUTPUT:
A single line of zeroes and ones.
For each word in the input, in order, output 1 if the word is a near-palindrome and 0 otherwise.

CONSTRAINTS:
0 < N < 1000
word length < 10000

EXAMPLE:
Input
2
ricecar
racecars

Output
11


### Notes:
Sample data files can be found in the "data" folder.


### Legal:
Copyright (C) 2022 Dann Moore.  All rights reserved.




