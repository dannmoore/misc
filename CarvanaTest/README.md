# CarvanaTest

### Overview:

Part of the interview process with Carvana involved a short Python exercise to create a small backend API that serves data.  This project uses the Flask web server microframework and is compatible with Python 3.0 or above.


### Usage:

Run this application with the following command:

> python.exe carvana.py

Then, visit the following address in a web browser:

> http://127.0.0.1:5000

The web application will then present you with a simple interface to test the backend API service.


### Notes:

Mock data is provided in the "Data" directory as .csv files.


### Legal:
Copyright (C) 2022 Dann Moore.  All rights reserved.




