#
# File: carvana.py
# Description: Carvana project
#
# Copyright (C) 2022 Dann Moore.  All rights reserved.
#
import datahelper	# A custom data handling script for locations and trips
import flask
from flask import request
from flask import send_from_directory


# Create Flask server application
app = flask.Flask(__name__)

# The following line enables Flask debugging
#app.config["DEBUG"] = True


# Read in location and trip data. (Note: This would likely be in a database, but for simplicity
#  we will load everything into memory at once)
datahelper.LoadLocationData("data/locations.csv")
datahelper.LoadTripData("data/trips.csv")


# Default route
@app.route('/', methods=['GET'])
def defaultRoute():
    # Send a static client html file
    return send_from_directory("./", "index.html")
#



# Route to get trips
@app.route('/get_trips', methods=['GET'])
def getTrips():
    # If location(s) were passed in the URL, process them
    if 'loc' in request.args:
        return datahelper.GetAllTrips(request.args['loc'])
    else:
        # Otherwise don't return data
        return "{}"
#


# Route to get a list of location codes
@app.route('/get_codes', methods=['GET'])
def getCodes():
    return datahelper.GetLocationCodeList()
#


# Route to get data for the specified of location code
@app.route('/get_code', methods=['GET'])
def getCode():
    # If a location was passed in the URL, process it
    if 'loc' in request.args:
        return datahelper.GetLocationCodeData(request.args['loc'])
    else:
        # Otherwise don't return data
        return "{}"
#


# Run the application
app.run()
