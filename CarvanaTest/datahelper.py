#
# File: datahelper.py
# Description: Data helper script for Carvana project
#
# Copyright (C) 2022 Dann Moore.  All rights reserved.
#
import json


# Create a location data dictionary
LocationData = {}

# Load location data from file into memory (Note: This assumes no duplicates exist in the file)
def LoadLocationData(filename):
    with open(filename) as f:
        # Read and skip the header line as it will not be used. For reference, the order of data
        #  in each line is: LocationCode, Latitude, Longitude, FacilityOwnedByCarvana        
        f.readline() 

        # For each additional line in the file
        for line in f:
            # Strip newline character from the end and split our line into an array
            tmp = line.rstrip().split(",")

            # Create a new LocationData element with the LocationCode (first element of the split string array) 
            #  as the key
            LocationData[tmp[0]] = {}
            LocationData[tmp[0]]['Latitude'] = tmp[1];
            LocationData[tmp[0]]['Longitude'] = tmp[2];
            LocationData[tmp[0]]['CarvanaOwned'] = tmp[3];
      
    f.close()
#






# Create a trip data dictionary
TripData = {}

# Load trip data from file into memory (Note: This assumes no duplicates exist in the file)
def LoadTripData(filename):
    with open(filename) as f:
        # Read and skip the header line as it will not be used. For reference, the order of data
        #  in each line is: Route, Origin, Destination, WeeklyCapacity       
        f.readline()    

        # For each additional line in the file
        for line in f:
            # Strip newline character from the end and split our line into an array
            tmp = line.rstrip().split(",")

            # Create a new LocationData element with the LocationCode (first element of the split string array) 
            #  as the key
            TripData[tmp[0]] = {}
            TripData[tmp[0]]['Origin'] = tmp[1];
            TripData[tmp[0]]['Destination'] = tmp[2];
            TripData[tmp[0]]['WeeklyCapacity'] = tmp[3];

    f.close()
#


# Returns location code data for a single location code
def GetLocationCodeData(location):
    output = {}

    # Verify code exists
    if location in LocationData:
        output[location] = LocationData[location]

    return json.dumps(output)
#


# Returns a list of all location codes
def GetLocationCodeList():
    output = {'Codes': []}

    # Extract just the location codes
    for code in LocationData:
        # Append code to the output
        output['Codes'].append(code)

    # Return data to the client
    return json.dumps(output)
#


# Returns trip data for a single specified location
def GetTripData(location):
    output = {}

    # Find all origin trips
    for trip in TripData:
        if TripData[trip]['Origin'] == location:
           # Copy this trip to output
           output[trip] = TripData[trip]

    # Find all destination trips
    for trip in TripData:
        if TripData[trip]['Destination'] == location:
           # Copy this trip to output
           output[trip] = TripData[trip]

    return output
#


# Returns trip data for all specified locations, given a comma-delimited
# string of location codes
def GetAllTrips(locations):
    tmp = locations.split(",")
    output = {}

    # Loop through each selected location
    for loc in tmp:
        # Retrieve trip data for the location
        data = GetTripData(loc)

        if len(data) > 0:
            # Append trip data to the output
            output[loc] = {}
            output[loc]['TripData'] = []
            output[loc]['TripData'].append(data)

            # Merge location code data into the output
            locdata = LocationData[loc]
            output[loc].update(locdata)

    return json.dumps(output)
#
